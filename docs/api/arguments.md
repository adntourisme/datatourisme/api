# Arguments

La requête **poi** accepte plusieurs paramètres destinés à modifier la sélection des résultats retournés :

```graphql
{
  poi(
    # <- arguments de la requête
  ) {
    # ...
  }
}
```

Voici la liste des arguments disponibles :

```graphql
{
  poi(
     uri: "http://...",  # <- Spécifie un identifiant de POI
     size: 10,          # <- Limite le nombre de résultats par page
     from: 0,            # <- Précise le début de page
     filters: [
       # ... <- Liste des filtres à appliquer pour sélectionner les POI
     ],
     sort: [
       # ... <- Liste des règles de tri des résultats
     ],
     lang: "fr"
  ) {
    # ...
  }
}
```

?> **Note** : L'ordre des arguments n'a pas d'importance.

## uri

L'argument **uri** permet de demander un POI en particulier, en précisant son URI. Le POI en question sera alors renvoyé
par l'API en respectant la structure des champs demandés dans le corps de la requête.

** Exemple **

```graphql
{
  poi(
    uri: "https://data.datatourisme.fr/31/27684064-5142-3a1f-ac20-9c1b656732c7"
  )
  {
    results{
      dc_identifier
    }
  }
}
```

Cette requête extrait tous les identifiants des **POI** dont l'URI est "https://data.datatourisme.fr/31/27684064-5142-3a1f-ac20-9c1b656732c7".

** Résultat **

```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO267549641513"
          ]
        }
      ]
    }
  }
}
```

## size, from

Les arguments **limit** et **from** permettent de gérer la pagination de la requête en limitant le nombre de résultats renvoyés par la requête et en se positionnant dans ces résultats.

?> Par défaut, si le champ **size** n'est pas spécifié, **le nombre de résultats renvoyés est de 10**.

```graphql
{
 poi(
     from: <position_résultats>,
     size: <nombre_de_résultats>
 )
 {
     # ...
 }
}
```

** Exemple **

```graphql
{
 poi(
     from: 5,
     size: 2
 )
 {
     results {
         dc_identifier
     }
 }
}
```

Cette requête extrait `2` identifiants de **POI** à partir du `cinquième` résultat renvoyé par la requête.

** Résultat **

```json
{
"data": {
 "poi": {
   "results": [
     {
       "dc_identifier": [
         "TFO311775817951"
       ]
     },
     {
       "dc_identifier": [
         "TFO309273189469"
       ]
     }
   ]
 }
}
}
```

## filters

L'argument **filters** permet de préciser une liste des filtres destinés à limiter les résultats en fonction de critères.

** Exemple **

```graphql
{
  poi(
    filters: [
      {
        allowedPersons: {
          _eq: 30
        }
      }
    ]
  )
  {
    results{
      _uri
    }
  }
}
```

La requête est ici limitée aux POI dont le nombre de personnes autorisées est égal à 30.

Le fonctionnement et la liste des filtres sont détaillés sur [la page dédiée aux filtres](api/filters/filters.md).

## sort

**sort** permet de trier les résultats en fonction d'une ou plusieurs propriétés.

```graphql
{
    poi(
        sort:[
            { <propriété>: { order: <ordre de tri> } }
        ]
    )
    {
        # ...
    }
}
```

L'**ordre de tri** peut être '**desc**' ou '**asc**'. Par défaut, l'ordre est aléatoire.

```graphql
{
    poi(
        sort:[
            { <propriété>: { order: "desc" } },
        ]
    )
    {
        # ...
    }
}
```

Les propriétés peuvent être atteintes en intégrant la hiérarchie dans le tri

```graphql
{
    poi(
        sort:[
            { <propriété1> : {<propriété2> : { order: <ordre de tri> } } }
        ]
    )
    {
        # ...
    }
}
```

Les tris peuvent être cumulés simplement :

```graphql
{
    poi(
        sort:[
            { <propriété1>: { order: "desc" } },
            { <propriété2>: { order: "asc" } }
        ]
    )
    {
        # ...
    }
}
```

** Exemple **

```graphql
{
 poi(
     sort: [{ allowedPersons: { order: "desc" }}]
 )
 {
     total
     results {
         dc_identifier
     }
 }
}
```

Cette requête extrait tous les identifiants des **POI** par ordre décroissant du nombre de personnes autorisées.

** Résultat **

```json
{
  "data": {
    "poi": {
      "total": 3,
      "results": [
        {
          "allowedPersons": [20]
        },
        {
          "allowedPersons": [20]
        },
        {
          "allowedPersons": [15]
        }
      ]
    }
  }
}
```

## lang

**lang** permet de limiter les propriétés traduisibles renvoyées dans la réponse aux
seules valeurs de la langue sélectionnée. La liste des codes acceptés est définie par
la norme **ISO 639-1** : [https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1].
