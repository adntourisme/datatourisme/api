# Champs

## Généralités

Le principe essentiel de GraphQL est d'adapter la réponse aux champs demandés par l'utilisateur dans le corps de la
requête :

```graphql
{
  poi {
    # ... <- corps de la requête
  }
}
```

Voici la liste des champs disponibles :

```graphql
{
  poi {
    total # <- nombre total de résultats
    results {
      # ... <- Champs et sous-champs d'un POI
    }
    query # <- requête SPARQL générée et exécutée par l'API
  }
}
```

C'est dans le champ **results** que l'utilisateur décrit précisément la structure des POI qu'il souhaite récupérer.
Cette structure doit respecter le format de l'ontologie.

L'[introspection GraphQL](http://graphql.org/learn/introspection/) vous permet de connaître précisément les champs et
sous-champs disponibles. Vous pouvez également consulter la [référence GraphQL](api/reference.md) de l'API, notamment
pour connaître la liste des champs d'un POI : [PointOfInterest](api/reference.md#pointofinterest).

?> **Note sur les namespaces RDF :** Les conventions RDF imposent le signe `:` entre le *prefix* du namespace et sa propriété ;
la spécification GraphQL n'autorisant pas cette notation, l'API DATAtourisme remplace le double point par un `_`.
Exemple: `rdfs:label` devient `rdfs_label`.

** Exemple **

```graphql
{
  poi {
    total
    results {
      _uri                  # <- Identifiant du POI
      dc_identifier         # <- Référence du POI
      isLocatedAt {
        schema_geo {
          schema_latitude   # <- Latitude du POI
          schema_longitude  # <- Longitude du POI
        }
      }
    }
  }
}
```

** Résultat **

```json
{
  "data": {
    "poi": {
      "total": 1308,
      "results": [
        {
          "_uri": "https://data.datatourisme.fr/21/5430055a-2d35-3ce7-adc9-ebecb9f5cbfd",
          "dc_identifier": ["FMACHA0100957599"],
          "isLocatedAt": [{
            "schema_geo": [{
              "schema_latitude": [
                49.376388888889
              ],
              "schema_longitude": [
                6.1580555555556
              ]
            }]
          }]
        },
        {
          "_uri": "https://data.datatourisme.fr/21/ce824246-dd4f-3f08-ab90-974a409f67d6",
          "dc_identifier": ["FMACHA0100957578"],
          "isLocatedAt": [{
            "schema_geo": [{
              "schema_latitude": [
                48.693055555556
              ],
              "schema_longitude": [
                6.1794444444445
              ]
            }]
          }]
        },
        {
          "_uri": "https://data.datatourisme.fr/31/00024f0a-74b7-34fd-80f0-eec5cbf61189",
          "dc_identifier": ["FMACHA0100705801"],
          "isLocatedAt": [{
            "schema_geo": [{
              "schema_latitude": [
                49.360329728326
              ],
              "schema_longitude": [
                6.1624977836914
              ]
            }]
          }]
        }
      ]
    }
  }
}
```

## Propriétés multilingues

Certaines propriétés (rdf:label, :shortDescription) sont multilingues : chaque valeur peut se retrouver traduite
en plusieurs langues.

Dans ce cas précis, leur structure est décomposée afin de connaître la langue associée à chacune des valeurs :

** Exemple **

```graphql
{
  poi {
    total
    results {
      _uri
      rdfs_label {
         value
         lang
      }
    }
  }
}
```

** Résultat **

```json
{
  "data": {
    "poi": {
      "total": 1308,
      "results": [
        {
          "_uri": "https://data.datatourisme.fr/21/5430055a-2d35-3ce7-adc9-ebecb9f5cbfd",
          "rdfs_label": [
            { "value": "Jouer aux échecs", lang: "fr" },
            { "value": "Play chess", lang: "en" }
          ]
        },
        {
          "_uri": "https://data.datatourisme.fr/21/ce824246-dd4f-3f08-ab90-974a409f67d6",
          "rdfs_label": [
            { "value": "Randonnée", lang: "fr" },
            { "value": "Hiking", lang: "en" }
          ]
        }
      ]
    }
  }
}
```
