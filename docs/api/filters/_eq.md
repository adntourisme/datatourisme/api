# _eq

Détermine une condition d'égalité. L'opérateur **_eq** extrait les POI dont la propriété visée est égale à la valeur fournie.

```graphql
{
    poi(
        filters: [
            { <propriété>: {_eq: <valeur>} }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```
### Comportement ###

- Egalité valeur de type scalaire :
l'égalité peut porter sur une valeur de type entier, réel, chaîne, booléen...

- Egalité valeur de type ressource :
l'égalité peut porter sur un **POI**, une personne, une adresse...

### Exemples ###

```graphql
{
  poi(
    filters: [
      {
        allowedPersons: {
          _eq: 30
        }
      }
    ]
  ) 
  {
    results{
      dc_identifier
    }   
  }
}
```
Cette requête extraira tous les identifiants des **POI** où le nombre de personnes autorisées (**allowedPersons**) est égal à 30. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO308495934640"
          ]
        }
      ]
    }
  }
}
```