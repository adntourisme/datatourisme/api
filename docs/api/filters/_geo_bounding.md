# _geo_bounding

**_geo_bounding** réalise une requête géographique sur la base d'une zone.
La requête renverra les POI situés dans une zone dont les coordonnées sont données en paramètres.

```graphql
{
    poi(
        filters: [
            { <propriété> { _geo_bounding: {sw_lng: <longitude bas gauche>, sw_lat:<latitude bas gauche>, ne_lng: <longitude haut droit>, ne_lat:<latitude haut droit> } }}}
        ]
    )
    {
        total
    }
}
```


### Exemples ###

```graphql
{
    poi(
        filters: [
            { isLocatedAt: {schema_geo: { _geo_bounding: {sw_lng: "2.55" , sw_lat:"44.33" , ne_lng: "2.58" , ne_lat:"44.36" } }}}
        ]
    )
    {
        total
    }
}
```
Cette requête extraira le nombre total de **POI** qui se situent dans cette zone géograpique. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "total": 1
    }
  }
}
```