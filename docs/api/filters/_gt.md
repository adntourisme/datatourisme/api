# _gt

Détermine une condition permettant une comparaison strictement supérieure. L'opérateur **_gt** extrait les POI dont la propriété visée est strictement supérieure à la valeur fournie.

```graphql
{
    poi(
        filters: [
            { <propriété>: {_gt: <valeur>} }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```

### Exemples ###

```graphql
{
  poi(
    filters: [
      {
        allowedPersons: {
          _gt: 30
        }
      }
    ]
  )
  {
    results{
      dc_identifier
    }
  }
}
```
Cette requête extraira tous les identifiants des **POI** où le nombre de personnes autorisées (**allowedPersons**) est supérieur à 30. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO130946193200"
          ]
        }
      ]
    }
  }
}
```