# _ne

Détermine une condition d'inégalité. L'opérateur **_ne** extrait les POI dont la propriété visée n'est pas égale à la valeur fournie.

```graphql
{
    poi(
        filters: [
            { <propriété>: {_ne: <valeur>} }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```

### Exemples ###

```graphql
{
  poi(
    filters: [
      {
        allowedPersons: {
          _ne: 30
        }
      }
    ]
  ) 
  {
    results{
      dc_identifier
    }
  }
}
```
Cette requête extraira tous les identifiants des **POI** dont le nombre de personnes autorisées (**allowedPersons**) est différent de 30. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO308495934640"
          ]
        }
      ]
    }
  }
}
```