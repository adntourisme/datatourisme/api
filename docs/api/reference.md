# Référence


  

  * [Query](#query)
  * [Objects](#objects)
    * [Agent](#agent)
    * [Amenity](#amenity)
    * [ArchitecturalStyle](#architecturalstyle)
    * [Audience](#audience)
    * [City](#city)
    * [CuisineCategory](#cuisinecategory)
    * [Department](#department)
    * [Description](#description)
    * [FeatureSpecification](#featurespecification)
    * [FoodProduct](#foodproduct)
    * [LangString](#langstring)
    * [Offer](#offer)
    * [PeopleAudience](#peopleaudience)
    * [Period](#period)
    * [Place](#place)
    * [PointOfInterest](#pointofinterest)
    * [PointOfInterest_ResultSet](#pointofinterest_resultset)
    * [PricingMode](#pricingmode)
    * [PricingPolicy](#pricingpolicy)
    * [Rating](#rating)
    * [Region](#region)
    * [Review](#review)
    * [ReviewSystem](#reviewsystem)
    * [RoomLayout](#roomlayout)
    * [SpatialEnvironmentTheme](#spatialenvironmenttheme)
    * [Statistics](#statistics)
    * [Theme](#theme)
    * [TourPath](#tourpath)
    * [ebucore_Annotation](#ebucore_annotation)
    * [ebucore_EditorialObject](#ebucore_editorialobject)
    * [ebucore_MimeType](#ebucore_mimetype)
    * [ebucore_Resource](#ebucore_resource)
    * [olo_Slot](#olo_slot)
    * [schema_Country](#schema_country)
    * [schema_DayOfWeek](#schema_dayofweek)
    * [schema_GenderType](#schema_gendertype)
    * [schema_GeoCoordinates_schema_GeoShape](#schema_geocoordinates_schema_geoshape)
    * [schema_OpeningHoursSpecification](#schema_openinghoursspecification)
    * [schema_PaymentMethod](#schema_paymentmethod)
    * [schema_PostalAddress](#schema_postaladdress)
    * [schema_PriceSpecification](#schema_pricespecification)
    * [schema_QuantitativeValue](#schema_quantitativevalue)
  * [Scalars](#scalars)
    * [Boolean](#boolean)
    * [DateTime](#datetime)
    * [Float](#float)
    * [Int](#int)
    * [String](#string)



## Query (datatourismeQuery)
<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>poi</strong></td>
<td valign="top"><a href="#/api/reference?id=pointofinterest_resultset">PointOfInterest_ResultSet</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">uri</td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">size</td>
<td valign="top"><a href="#/api/reference?id=int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">from</td>
<td valign="top"><a href="#/api/reference?id=int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">sort</td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest_Sort</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filters</td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest_Filter</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">lang</td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

## Objects

### Agent

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_comment</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Description de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_address</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_postaladdress">schema_PostalAddress</a>]</td>
<td>

L&#039;adresse postale du lieu concerné

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasCreated</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td>

Le POI que cet Agent a créé.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>owns</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasPublished</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td>

Le POI que cet Agent a publié.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>foaf_homepage</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

L&#039;adresse du site internet d&#039;un Agent.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>foaf_title</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

La civilité d&#039;une personne.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_gender</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_gendertype">schema_GenderType</a>]</td>
<td>

Le genre d&#039;une personne.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_faxNumber</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Un numéro de téléphone FAX.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_telephone</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Un numéro de téléphone.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_email</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Un courriel, courrier électronique.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_givenName</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le prénom d&#039;une personne.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_familyName</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le nom de famille d&#039;une personne.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>agreementLicense</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Numéro d&#039;immatriculation au registre des opérateurs de voyages,  pour les organismes qui font de la vente, ou de la distribution, de produits  touristiques.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>registeredNumber</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Numéro d’enregistrement, composé de 13 caractères, délivré par la  marie pour les meublés et chambres chez l’habitant qui ne répondraient pas  à la définition de la chambre d’hôtes.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>siret</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le numéro SIRET est un identifiant numérique de 14 chiffres composé  du SIREN (9 chiffres) et d&#039;un numéro interne de classement de 5 chiffres (NIC)  caractérisant l&#039;établissement d&#039;une entreprise en tant qu&#039;unité géographiquement  localisée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>apeNaf</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Code attribué par l&#039;INSEE en référence à la nomenclature des activité s françaises.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rcs</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Numéro d&#039;inscription au Registre du Commerce et des Sociétés.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_legalName</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le nom officiel de l&#039;organisation, ex: Le nom de la société enregistré e.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_logo</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Un logo représentant l&#039;organisation.

</td>
</tr>
</tbody>
</table>

### Amenity

Un équipement, un service, un aménagement, une infrastructure, proposé  par un POI. Ex: un minibar pour une chambre d&#039;hôtel, des bungalows pour un  camping, des couverts pour un restaurant.

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>availableLanguage</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Les langues proposées par l&#039;équipement d&#039;information. Il est recommandé  d&#039;utiliser comme valeur celles definies par la RFC 1766 qui comprend un code  de langage à deux caractères(venant du standard ISO 639), éventuellement suivi  d&#039;un code à deux lettres pour le pays (venant du standard ISO 3166). Par exemple,  &#039;en&#039; pour l&#039;anglais, &#039;fr&#039; pour le français, ou&#039;en-uk&#039; pour l&#039;anglais utilisé  au Royaume-Uni.

</td>
</tr>
</tbody>
</table>

### ArchitecturalStyle

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### Audience

Audience à laquelle s&#039;adresse l&#039;item. En général, il s&#039;agit d&#039;un  groupe d&#039;agents liés ensemble par un critère commun. ex: Enfants, Adolescents,  Adultes, Scolaires, ...

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>requiredMinPersonCount</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le nombre minimum de personnes requises pour que cette Audience  soit valide. Laisser cette propriété vide si aucun minimum n&#039;est requis.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>requiredMaxPersonCount</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le nombre maximum de personnes requises pour que l&#039;Audience soit  valide. Laisser cette propriété vide si aucun maximum n&#039;est requis.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_suggestedGender</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_gendertype">schema_GenderType</a>]</td>
<td>

Le genre suggéré.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_requiredMaxAge</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

L&#039;age maximum requis.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_suggestedMinAge</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

L&#039;age minimum suggéré.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_suggestedMaxAge</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

L&#039;age maximum suggéré.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_requiredGender</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_gendertype">schema_GenderType</a>]</td>
<td>

Le genre requis.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_requiredMinAge</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

L&#039;age minimum requis.

</td>
</tr>
</tbody>
</table>

### City

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isPartOfDepartment</strong></td>
<td valign="top">[<a href="#/api/reference?id=department">Department</a>]</td>
<td>

est une subdivision du département objet

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>insee</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Code INSEE de la division administrative

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### CuisineCategory

Le type de cuisine qu&#039;un établissement de restauration peut proposer.  Ex: &quot;Fruits de mer&quot;, &quot;Cuisin africaine&quot;.

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### Department

Les départements constituent la seconde division administrative  en France. Ex: Ain, Oise, ... Un département possède un code INSEE.

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isPartOfRegion</strong></td>
<td valign="top">[<a href="#/api/reference?id=region">Region</a>]</td>
<td>

est une subdivision de la région objet

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>insee</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Code INSEE de la division administrative

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### Description

Une description permet d&#039;associer la description textuelle d&#039;un  POI avec l&#039;Audience à qui cette description est dédiée. Par exemple, un POI  peut avoir une description dédiée aux écoles et une autre dédiée au grand  public. Si une description n&#039;a aucune audience renseignée, on suppose qu&#039;elle  est dédiée à tous les publics.

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dc_description</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Description longue de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDedicatedTo</strong></td>
<td valign="top">[<a href="#/api/reference?id=audience">Audience</a>]</td>
<td>

L&#039;audience à laquelle cet item est dédiée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortDescription</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Utiliser cette propriété pour attacher une description courte qui  pourra être utilisée sur des supports de publication où l&#039;espace est un critè re important.

</td>
</tr>
</tbody>
</table>

### FeatureSpecification

Permet d&#039;associer à un POI des équipements, service, infrastructures  ou aménagements en y aggrégeant des informations comme l&#039;accès internet, le  nombre d&#039;équipements proposés, si oui ou non cet équipement est inclus dans  les tarifs des services du produit, ...

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>features</strong></td>
<td valign="top">[<a href="#/api/reference?id=amenity">Amenity</a>]</td>
<td>

L&#039;équipement ou service auquel cette spécification se rapporte

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>charged</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si l&#039;équipement associé est payant (non inclus dans le tarif  du service).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_minValue</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur minimum de la donnée concernée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_value</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur de la donnée concernée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_maxValue</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur maximum de la donnée concernée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasFloorSize</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_quantitativevalue">schema_QuantitativeValue</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasLayout</strong></td>
<td valign="top">[<a href="#/api/reference?id=roomlayout">RoomLayout</a>]</td>
<td>

La disposition de la salle associée à cette spécification.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>occupancy</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

Le nombre maximum de personnes possibles sur la surface concerné e.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>airConditioning</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si cette surface propose l&#039;air conditionné

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>seatCount</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

Le nombre de places assises sur la surface concernée

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>internetAccess</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si le produit propose un accès à internet.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>noSmoking</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si cette surface est non fumeur.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>petsAllowed</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si les animaux sont acceptés dans ce lieu

</td>
</tr>
</tbody>
</table>

### FoodProduct

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### LangString

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td>

Literal value

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lang</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td>

Literal language

</td>
</tr>
</tbody>
</table>

### Offer

Une Offre permet de consommer un Product. ex : la location d&#039;une  chambre d&#039;hôtel, la visite d&#039;un musée, un cours de golf, ...
Une Offre peut  référencer une autre offre (un dîner au Restaurant peut être accessible via  la location d&#039;une chambre d&#039;hôtel).

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_priceSpecification</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_pricespecification">schema_PriceSpecification</a>]</td>
<td>

Les spécifications tarifaires de l&#039;offre concernée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_acceptedPaymentMethod</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_paymentmethod">schema_PaymentMethod</a>]</td>
<td>

Propose comme moyen de paiement pour l&#039;offre concernée.

</td>
</tr>
</tbody>
</table>

### PeopleAudience

Une audience composée d&#039;un groupe de personnes.

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>requiredMinPersonCount</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le nombre minimum de personnes requises pour que cette Audience  soit valide. Laisser cette propriété vide si aucun minimum n&#039;est requis.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>requiredMaxPersonCount</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le nombre maximum de personnes requises pour que l&#039;Audience soit  valide. Laisser cette propriété vide si aucun maximum n&#039;est requis.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_suggestedGender</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_gendertype">schema_GenderType</a>]</td>
<td>

Le genre suggéré.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_requiredMaxAge</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

L&#039;age maximum requis.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_suggestedMinAge</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

L&#039;age minimum suggéré.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_suggestedMaxAge</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

L&#039;age maximum suggéré.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_requiredGender</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_gendertype">schema_GenderType</a>]</td>
<td>

Le genre requis.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_requiredMinAge</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

L&#039;age minimum requis.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### Period

Une période est soit un intervalle de temps défini par une date  et heure de début et de fin (ex: du 14/07/2016 à 00h au 14/08/2016 à 12h),  soit une récurrence d&#039;intervalle (ex: tous les lundis et mercredi de 10h à  12h). Attention, Period n&#039;est pas similaire à schema:Event, car au sens schema.org,  un Event est une manifestation qui peut proposer des schema:Offers. Ici, Period  n&#039;est qu&#039;un intervalle de temps sans autre portée sémantique. 

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appliesOnDay</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_dayofweek">schema_DayOfWeek</a>]</td>
<td>

Le jour de la semaine auquel s&#039;applique cette période.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startTime</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

L&#039;heure à laquelle commence l&#039;occurence de la période (hh:mm).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>endDate</strong></td>
<td valign="top">[<a href="#/api/reference?id=datetime">DateTime</a>]</td>
<td>

Date à laquelle la période se termine, sans réccurrence interne   (aaaa-mm-jj).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>endTime</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

L&#039;heure à laquelle se termine l&#039;occurence de la période (hh:mm).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startDate</strong></td>
<td valign="top">[<a href="#/api/reference?id=datetime">DateTime</a>]</td>
<td>

Date à laquelle la période commence, sans récurrence interne (aaaa-mm-jj).

</td>
</tr>
</tbody>
</table>

### Place

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_address</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_postaladdress">schema_PostalAddress</a>]</td>
<td>

L&#039;adresse postale du lieu concerné

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_geo</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_geocoordinates_schema_geoshape">schema_GeoCoordinates_schema_GeoShape</a>]</td>
<td>

Les coordonnées géographiques de la ressource

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isCloseTo</strong></td>
<td valign="top">[<a href="#/api/reference?id=place">Place</a>]</td>
<td>

Un lieu géographiquement proche d&#039;un autre. La portée sémantique  de cette propriété est subjective.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_openingHoursSpecification</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_openinghoursspecification">schema_OpeningHoursSpecification</a>]</td>
<td>

Les heures d&#039;ouverture d&#039;un lieu/service/produit.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>airConditioning</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si cette surface propose l&#039;air conditionné

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>internetAccess</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si le produit propose un accès à internet.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>noSmoking</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si cette surface est non fumeur.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>altInsee</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Nom de la station balnéaire, station de ski... quand elle diffè re du nom officiel de la commune.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>petsAllowed</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si les animaux sont acceptés dans ce lieu

</td>
</tr>
</tbody>
</table>

### PointOfInterest

Tout objet touristique qui mérite d&#039;être décrit et valorisé. Un  POI (Point of Interest) est un élément touristique qui est géré par un Agent  et qui peut être consommé via des Produits et Services. ex : Un Restaurant,  un Hôtel, une Pratique, un Objet patrimonial

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le type de la ressource

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_comment</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Description de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isLocatedAt</strong></td>
<td valign="top">[<a href="#/api/reference?id=place">Place</a>]</td>
<td>

La localisation du POI, et donc le lieu où il peut être potentiellement  consommé.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasBookingContact</strong></td>
<td valign="top">[<a href="#/api/reference?id=agent">Agent</a>]</td>
<td>

L&#039;agent à contacter pour affaires de réservation relatives à ce  POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>suggests</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td>

Permet de relier un POI avec un autre POI similaire susceptible  de plaire au consommateur. Ex: Une piste de ski avec un loueur de skis.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasFeature</strong></td>
<td valign="top">[<a href="#/api/reference?id=featurespecification">FeatureSpecification</a>]</td>
<td>

La spécification des équipements et services que propose ce POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasTheme</strong></td>
<td valign="top">[<a href="#/api/reference?id=theme">Theme</a>]</td>
<td>

Le thème auquel se rapporte, ou qu&#039;évoque ce POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasPart</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td>

Un sous-produit de ce produit (par exemple si le produit est un  composant de celui-ci). Ex: un court de golf dans un camping.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>offers</strong></td>
<td valign="top">[<a href="#/api/reference?id=offer">Offer</a>]</td>
<td>

Moyens de paiement et politique tarifaire (A la journée, Par personne,  Tarifs..)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isAPartOf</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td>

Un produit dont ce produit est un composant. Ex: un camping pour  un court de golf.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasCommunicationContact</strong></td>
<td valign="top">[<a href="#/api/reference?id=agent">Agent</a>]</td>
<td>

L&#039;agent à contacter pour affaires liées à la communication liée  à ce POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isReferencedBy</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td>

Cette relation permet de relier deux POI entre eux lorsqu&#039;ils partagent  une relation logique.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasReview</strong></td>
<td valign="top">[<a href="#/api/reference?id=review">Review</a>]</td>
<td>

Un classement qui évalue ce POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isOwnedBy</strong></td>
<td valign="top">[<a href="#/api/reference?id=agent">Agent</a>]</td>
<td>

Le propriétaire de ce POI, ou a minima celui qui en propose les  services.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasManagementContact</strong></td>
<td valign="top">[<a href="#/api/reference?id=agent">Agent</a>]</td>
<td>

L&#039;agent à contacter pour affaires liées à la direction / gestion  de ce POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasDescription</strong></td>
<td valign="top">[<a href="#/api/reference?id=description">Description</a>]</td>
<td>

Description textuelle courte ou longue du POI pouvant être associé e à une audience. Par exemple, un POI peut avoir une description dédiée aux  écoles et une autre dédiée au grand public.
Si une description n&#039;a aucune  audience renseignée, on suppose qu&#039;elle est dédiée à tous les publics.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasMainRepresentation</strong></td>
<td valign="top">[<a href="#/api/reference?id=ebucore_editorialobject">ebucore_EditorialObject</a>]</td>
<td>

Un média considéré comme représentation principale du POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasBeenCreatedBy</strong></td>
<td valign="top">[<a href="#/api/reference?id=agent">Agent</a>]</td>
<td>

L&#039;agent qui a créé ce POI dans le système d&#039;information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isEquippedWith</strong></td>
<td valign="top">[<a href="#/api/reference?id=amenity">Amenity</a>]</td>
<td>

L&#039;équipement ou service que ce POI propose.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasNeighborhood</strong></td>
<td valign="top">[<a href="#/api/reference?id=spatialenvironmenttheme">SpatialEnvironmentTheme</a>]</td>
<td>

Le thème environmental de POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasRepresentation</strong></td>
<td valign="top">[<a href="#/api/reference?id=ebucore_editorialobject">ebucore_EditorialObject</a>]</td>
<td>

Une représentation est un Media qui est lié à un POI. ex : une photo  d&#039;un POI, un document PDF promotionnel, ...

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasContact</strong></td>
<td valign="top">[<a href="#/api/reference?id=agent">Agent</a>]</td>
<td>

L&#039;agent à contacter pour affaires générales relatives à ce POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasBeenPublishedBy</strong></td>
<td valign="top">[<a href="#/api/reference?id=agent">Agent</a>]</td>
<td>

L&#039;agent qui a publié ce POI dans le système d&#039;information.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasAdministrativeContact</strong></td>
<td valign="top">[<a href="#/api/reference?id=agent">Agent</a>]</td>
<td>

L&#039;agent à contacter pour affaires administratives relatives à ce  POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastUpdate</strong></td>
<td valign="top">[<a href="#/api/reference?id=datetime">DateTime</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>allowedPersons</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

Nombre de personnes pouvant être accueillies dans le POI

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>availableLanguage</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Les langues proposées par l&#039;équipement d&#039;information. Il est recommandé  d&#039;utiliser comme valeur celles definies par la RFC 1766 qui comprend un code  de langage à deux caractères(venant du standard ISO 639), éventuellement suivi  d&#039;un code à deux lettres pour le pays (venant du standard ISO 3166). Par exemple,  &#039;en&#039; pour l&#039;anglais, &#039;fr&#039; pour le français, ou&#039;en-uk&#039; pour l&#039;anglais utilisé  au Royaume-Uni.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reducedMobilityAccess</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si le produit propose un accès aux personnes à mobilité ré duite

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>creationDate</strong></td>
<td valign="top">[<a href="#/api/reference?id=datetime">DateTime</a>]</td>
<td>

La date de création de la ressource n&#039;est pas privilégiée dans le  système, se baser davantage sur la date de publication : lastUpdate (aaaa-mm-jj)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dc_identifier</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Identifiant de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>takesPlaceAt</strong></td>
<td valign="top">[<a href="#/api/reference?id=period">Period</a>]</td>
<td>

La période sur laquelle se déroule cet événement.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasTourType</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le type d&#039;itinéraire : A/R, bloucle, itinérance.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>meets</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td>

Le POI dont ce parcours croise la route.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>minAltitude</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

L&#039;altitude du point le plus bas sur le parcours.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAltitude</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

L&#039;altitude du point le plus haut sur le parcours. Exprimée en mè tres.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tourDistance</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

La distance entre le début et la fin d&#039;une étape ou du parcours  entier. Exprimée en mètres.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>duration</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

La durée estimée du parcours ou de l&#039;étape. Exprimée en minutes.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>highDifference</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Le dénivelé du parcours (différence entre l&#039;altitude minimale et  l&#039;altitude maximale). Exprimée en mètres.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isConsumableFor</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td>

Permet de référencer un produit qui est un consommable d&#039;un POI.  Le consommable ne peut avoir d&#039;existence propre (si le POI cible est supprimé , le consommable doit l&#039;être également).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>olo_slot</strong></td>
<td valign="top">[<a href="#/api/reference?id=olo_slot">olo_Slot</a>]</td>
<td>

Un itinéraire peut être composé d&#039;une ou plusieurs étapes.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasArchitecturalStyle</strong></td>
<td valign="top">[<a href="#/api/reference?id=architecturalstyle">ArchitecturalStyle</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>providesCuisineOfType</strong></td>
<td valign="top">[<a href="#/api/reference?id=cuisinecategory">CuisineCategory</a>]</td>
<td>

Le type de cuisine de cet établissement de restauration.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>takeAway</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>allYouCanEat</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>guided</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasFloorSize</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_quantitativevalue">schema_QuantitativeValue</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>providesFoodProduct</strong></td>
<td valign="top">[<a href="#/api/reference?id=foodproduct">FoodProduct</a>]</td>
<td>

Le type de mêts qui est proposé à la dégustation.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>saleOnSite</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si la dégustation propose une vente à la propriété.

</td>
</tr>
</tbody>
</table>

### PointOfInterest_ResultSet

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#/api/reference?id=int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>results</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>query</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>stats</strong></td>
<td valign="top"><a href="#/api/reference?id=statistics">Statistics</a></td>
<td></td>
</tr>
</tbody>
</table>

### PricingMode

Le mode de tarification. ex : Abonnement, à la journée, par personne,  ...

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### PricingPolicy

Une politique de tarification détermine les conditions de consommation  que couvre ce tarif. Ex: demi-pension, pension complète (Hôtellerie), à la  semaine (Location), plein tarif (Générique), à la carte, menu adulte (Restauration),  ...

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### Rating

Valeur associée à un Classement.

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isRatingProvidedBy</strong></td>
<td valign="top">[<a href="#/api/reference?id=reviewsystem">ReviewSystem</a>]</td>
<td>

Le système de classement qui propose cette valeur.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### Region

Les régions constituent la première division adminsitrative en France.  Ex: Bretagne Guadeloupe, Auvergne-Rhône-Alpes, ... Une région possède un code  INSEE.

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isPartOfCountry</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_country">schema_Country</a>]</td>
<td>

est une subdivision du pays objet

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>insee</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Code INSEE de la division administrative

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### Review

Un classement permet d&#039;associer une valeur de classement à un POI.  Ex: 3 étoiles, 4 Epis, &quot;Plus beau village de France&quot;, ...

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasReviewValue</strong></td>
<td valign="top">[<a href="#/api/reference?id=rating">Rating</a>]</td>
<td>

La valeur d&#039;un classement.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reviewDeliveryDate</strong></td>
<td valign="top">[<a href="#/api/reference?id=datetime">DateTime</a>]</td>
<td>

La date à laquelle le classement a été délivré (aaaa-mm-jj).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pending</strong></td>
<td valign="top">[<a href="#/api/reference?id=boolean">Boolean</a>]</td>
<td>

Vrai si le classement est en cours.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_datePublished</strong></td>
<td valign="top">[<a href="#/api/reference?id=datetime">DateTime</a>]</td>
<td>

La date de la première publication.

</td>
</tr>
</tbody>
</table>

### ReviewSystem

Un système de classement décrit les valeurs possibles d&#039;un classement  donné. Un système de classement est généralement éligible à un type de POI  donné. ex:  Guide Michelin, Plus beaux villages de France, ...

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>allowsRating</strong></td>
<td valign="top">[<a href="#/api/reference?id=rating">Rating</a>]</td>
<td>

Les valeurs que ce système de classement autorise.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reviewValidityDuration</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

La durée de validité d&#039;un classement, depuis sa reviewDeliveryDate.  Exprimée en nombre de jours.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>bestRating</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

Le score le plus haut que le système de classement propose.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>worstRating</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

La pire valeur que ce système de classement propose.

</td>
</tr>
</tbody>
</table>

### RoomLayout

La disposition de la salle. Ex: en banquet, en U, en cocktail, ...

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### SpatialEnvironmentTheme

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### Statistics

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>execution</strong></td>
<td valign="top"><a href="#/api/reference?id=float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>count</strong></td>
<td valign="top"><a href="#/api/reference?id=float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hydration</strong></td>
<td valign="top"><a href="#/api/reference?id=float">Float</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#/api/reference?id=float">Float</a></td>
<td></td>
</tr>
</tbody>
</table>

### Theme

Un thème est un sujet auquel un POI peut se rapporter. Ex: la peinture,  l&#039;histoire, le textile, ...

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### TourPath

Le détail d&#039;une étape d&#039;itinéraire.

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dc_description</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Description longue de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isLocatedAt</strong></td>
<td valign="top">[<a href="#/api/reference?id=place">Place</a>]</td>
<td>

La localisation du POI, et donc le lieu où il peut être potentiellement  consommé.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasMainRepresentation</strong></td>
<td valign="top">[<a href="#/api/reference?id=ebucore_editorialobject">ebucore_EditorialObject</a>]</td>
<td>

Un média considéré comme représentation principale du POI.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasHikeAudience</strong></td>
<td valign="top">[<a href="#/api/reference?id=audience">Audience</a>]</td>
<td>

L&#039;audience cible de cet itinéraire.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasRepresentation</strong></td>
<td valign="top">[<a href="#/api/reference?id=ebucore_editorialobject">ebucore_EditorialObject</a>]</td>
<td>

Une représentation est un Media qui est lié à un POI. ex : une photo  d&#039;un POI, un document PDF promotionnel, ...

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>meets</strong></td>
<td valign="top">[<a href="#/api/reference?id=pointofinterest">PointOfInterest</a>]</td>
<td>

Le POI dont ce parcours croise la route.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>minAltitude</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

L&#039;altitude du point le plus bas sur le parcours.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxAltitude</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

L&#039;altitude du point le plus haut sur le parcours. Exprimée en mè tres.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tourDistance</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

La distance entre le début et la fin d&#039;une étape ou du parcours  entier. Exprimée en mètres.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>duration</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

La durée estimée du parcours ou de l&#039;étape. Exprimée en minutes.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>highDifference</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Le dénivelé du parcours (différence entre l&#039;altitude minimale et  l&#039;altitude maximale). Exprimée en mètres.

</td>
</tr>
</tbody>
</table>

### ebucore_Annotation

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_comments</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

La description de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_abstract</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

fournit un description courte.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>credits</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_title</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Titre de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_isCoveredBy</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Les droits applicables sur le media.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDedicatedTo</strong></td>
<td valign="top">[<a href="#/api/reference?id=audience">Audience</a>]</td>
<td>

L&#039;audience à laquelle cet item est dédiée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rightsStartDate</strong></td>
<td valign="top">[<a href="#/api/reference?id=datetime">DateTime</a>]</td>
<td>

La date à laquelle les droits commencent à s&#039;appliquer sur ce mé dia (aaaa-mm-jj).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dct_hasVersion</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Une ressource associée qui est une version, une édition ou une adaptation  de la ressource décrite.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rightsEndDate</strong></td>
<td valign="top">[<a href="#/api/reference?id=datetime">DateTime</a>]</td>
<td>

La date à laquelle les droits arrêtent de s&#039;appliquer sur ce mé dia (aaaa-mm-jj).

</td>
</tr>
</tbody>
</table>

### ebucore_EditorialObject

Une ressource réprésentant l&#039;object concerné.

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_hasAnnotation</strong></td>
<td valign="top">[<a href="#/api/reference?id=ebucore_annotation">ebucore_Annotation</a>]</td>
<td>

Une annotation à propos de la ressource concernée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_hasRelatedResource</strong></td>
<td valign="top">[<a href="#/api/reference?id=ebucore_resource">ebucore_Resource</a>]</td>
<td>

A pour media représentatif.

</td>
</tr>
</tbody>
</table>

### ebucore_MimeType

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### ebucore_Resource

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_hasMimeType</strong></td>
<td valign="top">[<a href="#/api/reference?id=ebucore_mimetype">ebucore_MimeType</a>]</td>
<td>

le type mime de la ressource (MimeType).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_widthUnit</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

L&#039;unité de la largeur du media.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_locator</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

L&#039;adresse URL de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_width</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

La largeur du media.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_heightUnit</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

L&#039;unité de la hauteur du media.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_fileSize</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

La taille du fichier de la ressource. (en octet)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ebucore_height</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

La hauteur du media.

</td>
</tr>
</tbody>
</table>

### olo_Slot

Une étape d&#039;itinéraire

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>olo_item</strong></td>
<td valign="top">[<a href="#/api/reference?id=tourpath">TourPath</a>]</td>
<td>

Le contenu de l&#039;élément de la liste ordonnancée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>olo_index</strong></td>
<td valign="top">[<a href="#/api/reference?id=int">Int</a>]</td>
<td>

a comme ordre de déroulement

</td>
</tr>
</tbody>
</table>

### schema_Country

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### schema_DayOfWeek

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### schema_GenderType

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### schema_GeoCoordinates_schema_GeoShape

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_longitude</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur de la longitude de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_elevation</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur de l&#039;altitude de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_latitude</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur de la latitude de la ressource.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_line</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Une ligne est un chemin point à point composé de deux points ou  plus. Une ligne est exprimée sous la forme d&#039;une série de deux ou plusieurs  objets point séparés par un caractère espace.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_polygon</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Un polygone est la zone entourée par un chemin point-à-point pour  lequel les points de départ et de fin sont identiques. Un polygone est exprimé  sous la forme d&#039;une série de quatre points délimités par espace ou plus où  le premier et le dernier point sont identiques.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_circle</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Un cercle est la zone circulaire d&#039;un rayon spécifié, centré à une  latitude et à une longitude spécifiées. Un cercle est exprimé en paire suivi  d&#039;un rayon en mètres.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_box</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Une boîte est une zone définie par le rectangle formé par deux points.  Le premier point est le coin inférieur, le deuxième point est le coin supé rieur. Une boîte est exprimée en deux points séparés par un caractère espace.

</td>
</tr>
</tbody>
</table>

### schema_OpeningHoursSpecification

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_dayOfWeek</strong></td>
<td valign="top">[<a href="#/api/reference?id=schema_dayofweek">schema_DayOfWeek</a>]</td>
<td>

Le jour de la semaine pour laquelle ces heures d&#039;ouverture sont  valides.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_closes</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

L&#039;heure de fermeture du lieu ou du service le (s) jour (s) donné  (s) de la semaine.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>additionalInformation</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_validThrough</strong></td>
<td valign="top">[<a href="#/api/reference?id=datetime">DateTime</a>]</td>
<td>

La date de fin de la période d&#039;ouverture (aaaa-mm-jjThh:mm:ss).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_validFrom</strong></td>
<td valign="top">[<a href="#/api/reference?id=datetime">DateTime</a>]</td>
<td>

La date de début de la période d&#039;ouverture (aaaa-mm-jjThh:mm:ss).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_opens</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

L&#039;heure d&#039;ouverture du lieu ou du service le (s) jour (s) donné  (s) de la semaine.

</td>
</tr>
</tbody>
</table>

### schema_PaymentMethod

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdfs_label</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td>

Etiquette courte décrivant la ressource.

</td>
</tr>
</tbody>
</table>

### schema_PostalAddress

Adresse postale

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_addressLocality</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

La ville en format texte.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_postalCode</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le code postal d&#039;une adresse.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_streetAddress</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le nom de rue incluant le numéro d&#039;une adresse.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_postOfficeBoxNumber</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le numéro de la boîte postale.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasAddressCity</strong></td>
<td valign="top">[<a href="#/api/reference?id=city">City</a>]</td>
<td>

La localité de cette addresse (code INSEE de la commune)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cedex</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Le CEDEX (ou Courrier d&#039;Entreprise à Distribution EXceptionnelle),  composé de 5 chiffres.

</td>
</tr>
</tbody>
</table>

### schema_PriceSpecification

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_eligibleQuantity</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

La quantité disponible pour ce tarif.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_maxPrice</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur du tarif maximum.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_priceCurrency</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

La devise en 3 lettres (format ISO 4217). Exemple: EUR .

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>additionalInformation</strong></td>
<td valign="top">[<a href="#/api/reference?id=langstring">LangString</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_price</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur du tarif.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_minPrice</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur du tarif minimum.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appliesOnPeriod</strong></td>
<td valign="top">[<a href="#/api/reference?id=period">Period</a>]</td>
<td>

La période sur laquelle s&#039;applique ce tarif.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasEligiblePolicy</strong></td>
<td valign="top">[<a href="#/api/reference?id=pricingpolicy">PricingPolicy</a>]</td>
<td>

La politique de prix qui s&#039;applique (En demi-pension...).

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasEligibleAudience</strong></td>
<td valign="top">[<a href="#/api/reference?id=peopleaudience">PeopleAudience</a>]</td>
<td>

Le type de public auquel ce prix s&#039;applique. ex: Adolescents, Adultes,  ...

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasPricingMode</strong></td>
<td valign="top">[<a href="#/api/reference?id=pricingmode">PricingMode</a>]</td>
<td>

Mode d&#039;application du tarif. Des exemples de mode d&#039;application  de tarif sont : A la journée, Par personne, etc.

</td>
</tr>
</tbody>
</table>

### schema_QuantitativeValue

Une valeur associée à une unité.

<table>
<thead>
<tr>
<th align="left" colspan="2">Field</th>

<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>_uri</strong></td>
<td valign="top"><a href="#/api/reference?id=string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rdf_type</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_minValue</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur minimum de la donnée concernée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_unitText</strong></td>
<td valign="top">[<a href="#/api/reference?id=string">String</a>]</td>
<td>

Précise l&#039;unité de la valeur associée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_value</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur de la donnée concernée.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>schema_maxValue</strong></td>
<td valign="top">[<a href="#/api/reference?id=float">Float</a>]</td>
<td>

Valeur maximum de la donnée concernée.

</td>
</tr>
</tbody>
</table>

## Scalars

### Boolean

The `Boolean` scalar type represents `true` or `false`.

### DateTime

Representation of date and time in "Y-m-d H:i:s" format

### Float

The `Float` scalar type represents signed double-precision fractional values as specified by [IEEE 754](http://en.wikipedia.org/wiki/IEEE_floating_point).

### Int

The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^53 - 1) and 2^53 - 1 since represented in JSON as double-precision floating point numbers specifiedby [IEEE 754](http://en.wikipedia.org/wiki/IEEE_floating_point).

### String

The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.

