# Resolver

Datatourisme API peut s'interfacer avec différentes sources de données. 
Par défaut, celle-ci interroge le système de gestion de base de données graphe Blazegraph.
Il est cependant tout à fait possible de configurer Datatourisme API pour interroger des endpoints de type
Virtuoso ou encore HDT et, plus généralement, tout type d'endpoint [SPARQL](https://fr.wikipedia.org/wiki/SPARQL).

## Blazegraph

```php

$resolver = new BlazegraphResolver('http://localhost:9999/blazegraph/namespace/kb/sparql');
$api = DatatourismeApi::create($resolver);

// On peut immédiatement requêter le endpoint avec GraphQL:
var_dump($api->process('{poi(filters:[{allowedPersons:{_eq:30}}]) {results{dc_identifier}}}'));
```
La connexion à Blazegraph étant celle établie par défaut, il suffit de préciser 
l'url de l'endpoint de Blazegraph pour requêter directement celui-ci ; le code suivant est donc équivalent au précédent :
```php
$api = DatatourismeApi::create('http://localhost:9999/blazegraph/namespace/kb/sparql');

var_dump($api->process('{poi(filters:[{allowedPersons:{_eq:30}}]) {results{dc_identifier}}}'));
```

## Virtuoso
[Virtuoso](https://www.virtuoso.com/) est une base de données hybride capable d'interroger ses données à l'aide de SPARQL.
Virtuoso est donc compatible avec la Datatourisme API
```php
$resolver = new VirtuosoResolver('http://localhost:8890/sparql/');
$api = DatatourismeApi::create($resolver);

// On peut immédiatement requêter l'endpoint avec GraphQL :
var_dump($api->process('{poi(filters:[{allowedPersons:{_eq:30}}]) {results{dc_identifier}}}'));
```

## HDT
[HDT](http://www.rdfhdt.org/) est une structure de fichier au format RDF. 
La base de données se résume donc à un fichier possédant une extension .HDT.
Pour l'api Datatourisme, le endpoint sera dans ce cas le chemin vers le fichier HDT.
```php
$resolver = new HdtResolver('/chemin/vers/le/fichier.hdt');
$api = DatatourismeApi::create($resolver);

// On peut immédiatement requêter l'endpoint avec GraphQL :
var_dump($api->process('{poi(filters:[{allowedPersons:{_eq:30}}]) {results{dc_identifier}}}'));
```
