# Démarrage rapide

Afin d'utiliser l'API DATAtourisme, il est nécessaire de charger les données issues de la
[plateforme diffuseur](https://diffuseur.datatourisme.fr) dans une base de données sémantiques. Cette section décrit
toutes les étapes nécessaires :

1. Récupération d'un fichier de données
2. Mise en place d'une base de données Blazegraph
3. Chargement des données DATAtourisme dans la base de données
4. Installation et utilisation de l'API

## Pré-requis

- [PHP 7.0+](http://php.net)
- [Curl](https://curl.haxx.se/download.html)
- [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)

## Récupération d'un fichier de données

Pour vous servir de l'API DATAtourisme, vous devez d'abord récupérer des données touristiques :

1. Connectez-vous à la [plateforme diffuseur](https://diffuseur.datatourisme.fr). Il sera nécessaire de
créer un compte s'il s'agit de votre première connexion.
2. Créez et configurez un flux de données à l'aide de l'éditeur visuel de requête.
3. Configurez le flux pour utiliser un format **compatible avec l'API** :
    * RDF-XML
    * Turtle
    * NT
4. Téléchargez le fichier du flux une fois celui-ci disponible.

Le fichier ainsi obtenu constitue votre source de données : il devra être chargé dans une base de données sémantique.

## Mise en place d'une base de données Blazegraph

[Blazegraph](http://www.blazegraph.com) est un système de gestion de base de données sémantiques open-source. Il met
à disposition un point d'accès SPARQL permettant à l'API d'interroger son contenu pour répondre aux requêtes GraphQL.

La version JAR 2.1.5 de Blazegraph peut être téléchargée à l'adresse suivante :
[blazegraph 2.1.5](https://github.com/blazegraph/database/releases/download/BLAZEGRAPH_RELEASE_CANDIDATE_2_1_5/blazegraph.jar)

```bash
curl -L -o blazegraph.jar https://github.com/blazegraph/database/releases/download/BLAZEGRAPH_RELEASE_CANDIDATE_2_1_5/blazegraph.jar
```

Le lancement du serveur Blazegraph se fait simplement par :

```bash
java -jar blazegraph.jar
```

Le serveur blazegraph est alors lancé et son interface d'administration est accessible depuis l'adresse http://localhost:9999/

## Chargement des données

Le fichier téléchargé lors de l'étape précédente pouvant être de différents formats, la commande pour charger les données dans Blazegraph peut varier :

```bash
# format RDF-XML (extension RDF)
curl -X POST -H "Content-Type:application/rdf+xml" --data-binary @fichier.rdf "http://localhost:9999/blazegraph/namespace/kb/sparql"

# format Turtle (extension TTL)
curl -X POST -H "Content-Type:application/x-turtle" --data-binary @fichier.ttl "http://localhost:9999/blazegraph/namespace/kb/sparql"

# format N-Triples (extension NT)
curl -X POST -H "Content-Type:text/plain" --data-binary @fichier.nt "http://localhost:9999/blazegraph/namespace/kb/sparql"
```

Votre base de données sémantiques est prête à être interrogée par l'API !

## Installation de l'API

La méthode recommandée pour installer l'API DATAtourisme est d'utilisr [Composer](https://getcomposer.org/) :

```bash
composer require datatourisme/api
```

## Utilisation de l'API

Voici un exemple d'utilisation de l'API :

```php
// composer autoload
require __DIR__ . '/vendor/autoload.php';

// instanciation du client
$api = \Datatourisme\Api\DatatourismeApi::create('http://localhost:9999/blazegraph/namespace/kb/sparql');

// éxecution d'une requête
$result = $api->process('{poi { results{ rdfs_label } } }');

// prévisualisation des résultats
var_dump($result);
```
