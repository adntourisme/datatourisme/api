# Principes de l'API

## Comment ça fonctionne ?

L'API Datatourisme est une librairie PHP permettant à l'utilisateur d'interroger facilement une base de données
sémantiques contenant des données touristiques issues de la plateforme DATAtourisme. Elle s'appuie sur un langage de
requête [GraphQL](https://graphql.org) dont le schéma de données est fondé sur l'ontologie DATAtourisme.

Afin de mettre en œuvre l'API, l'utilisateur doit donc :

1. Récupérer les données DATAtourisme au format sémantique sur la [plateforme diffuseur](https://diffuseur.datatourisme.fr)
2. Charger les données sémantiques dans une base de données sémantiques (triplestore) disposant d'un point d'accès SPARQL
3. Utiliser l'API DATAtourisme pour soumettre les requêtes GraphQL à la base de données et récupérer les résultats

Il existe quelques implémentations open-source de base de données sémantiques :

* [Blazegraph](https://www.blazegraph.com)
* [Virtuoso](https://github.com/openlink/virtuoso-opensource)
* [Jena/TDB](https://jena.apache.org/documentation/tdb/)

?> **A noter** : vous pouvez directement télécharger un environnement complet Docker incluant l'API, une base de données
Blazegraph et des outils annexes à l'adresse suivante : https://framagit.org/datatourisme/docker-stack

!> **IMPORTANT** : L'API DATAtourisme n'interroge pas directement la base de données nationale : elle vise à interroger
les données récupérées par l'utilisateur au travers de la [plateforme diffuseur](https://diffuseur.datatourisme.fr)
et chargées dans une base de données sémantiques locale.

## Utilisation de graphQL

Comme évoqué, l'API DATAtourisme s'appuie sur le langage [GraphQL](http://graphql.org/) pour interroger les données
touristiques. Les données sont renvoyées par l'API au format JSON.

Il s'agit d'un langage de requête **par l'exemple** : la structure retournée correspond à celle formulée dans la
requête. Ainsi, la quantité de données interrogées et transférées est réduite au strict nécessaire.

** Exemple de requête simple :**

```graphql
{
  poi {
    results {
      _uri                  # <- Identifiant du POI
      rdfs_label            # <- Nom du POI
      hasDescription {
        shortDescription    # <- Description du POI
      }
    }  
  }
}
```
Avec cette requête, la base de données sémantiques est interrogée pour extraire l'URI, le nom et la description de
l'ensemble des POI qu'elle contient. Le résultat au format JSON doit ressembler à :

```json
{
  "data": {
    "poi": {
      "results": [
        {
          "_uri": "https://data.datatourisme.fr/21/5430055a-2d35-3ce7-adc9-ebecb9f5cbfd",
          "rdfs_label": ["Jouer aux échecs"],
          "hasDescription": [{
              "shortDescription": [
                "Rendez-vous le 1er et le 3ème vendredi du mois à la maison des Associations, cours apprentissage, perfectionnement, rencontres amicales, tournois."
              ]
          }]
        },
        {
          "_uri": "https://data.datatourisme.fr/21/ce824246-dd4f-3f08-ab90-974a409f67d6",
          "rdfs_label": ["Le sens de la fête"],
          "hasDescription": [{
              "shortDescription": [
                "Comédie de Eric Toledano et Olivier Nakache. Avec Jean-Pierre Bacri, Gilles Lellouche, Eye Haidara."
              ]
            }
        ]},
        {
          "_uri": "https://data.datatourisme.fr/31/00024f0a-74b7-34fd-80f0-eec5cbf61189",
          "rdfs_label": ["Randonnée"],
          "hasDescription": [{
              "shortDescription": [
                "Randonnée par les Randonneurs du Montet"
              ]
          }]
        }
      ]
    }
  }
}
```

Pour en savoir plus sur les champs disponibles, les options de filtres et de pagination, vous pouvez consulter la
rubrique associée : [Utilisation de l'API](api/fields.md).

Pour en savoir plus sur le langage GraphQL, la documentation en ligne est disponible sur le site de
[GraphQL](http://graphql.org/)
