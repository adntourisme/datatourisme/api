# Le projet DATAtourisme

<p align="center">
  <a href="https://www.datatourisme.fr" target="_blank">
    <img alt="DATAtourisme" src="./_media/logo.png">
  </a>
</p>

DATAtourisme est un projet de R&D lauréat du Programme d’Investissements d’Avenir (PIA). Il vise à rassembler au sein
d’une plateforme nationale les données d’information touristique produites par les Offices de Tourisme,
Agences Départementales et Comités Régionaux du Tourisme, afin de les diffuser en open-data et ainsi faciliter la
création de services touristiques innovants par des start-ups, agences digitales, médias et autres acteurs publics ou
privés.

Le projet DATAtourisme a pour mission de :

* **Agréger** les données touristiques à partir des systèmes d'information touristique
* **Normaliser** les données par l'utilisation d'un vocabulaire touristique commun
* **Stocker** les données au sein d'une base nationale
* **Diffuser** les données auprès des réutilisateurs et diffuseurs d'information touristique


[En savoir plus sur le projet](http://www.datatourisme.fr)

## Format des données

Le projet DATAtourisme s'inscrit dans une démarche d'ouverture des données. Cette démarche a été définie par le
Consortium W3C dans la perspective du Web sémantique ou Web de données (Linked Data).

[En savoir plus sur le web sémantique](https://fr.wikipedia.org/wiki/Web_s%C3%A9mantique)

Il s'agit de structurer les points d'intérêt touristiques pour que les services tiers puissent mieux les exploiter.
Les données aggrégées par DATAtourisme sont ainsi restructurées, regroupées, enrichies par des traitements
automatiques, puis publiées selon le format descriptif du web sémantique : **RDF**.

[En savoir plus sur RDF](https://fr.wikipedia.org/wiki/Resource_Description_Framework)

Les données sont disponibles sur [la plateforme diffuseur](https://diffuseur.datatourisme.fr), selon plusieurs
syntaxes RDF : RDF/XML, N-Triples, Turtle, JSON-LD...

** Ce que les données contiennent :**

* **Une URI pour chaque point d'intérêt** : tout point d'intérêt dispose d’un identifiant pérenne, attribué
selon son producteur respectif.
* **Pour chaque point d'intérêt, un ensemble de propriétés** associées à l’URI de la ressource et des sous-ressources
sous forme de **triplets RDF**, selon les technologies du web sémantique et du linked open data.

## Ontologie

Pour normaliser l'ensemble des points d'intérêt stockés, DATAtourisme s'appuie sur une ontologie nationale de
description de l'offre touristique. Cette ontologie est publiée au format OWL, selon les principes du web sémantique.

Cette ontologie se compose :

* d'une ontologie "tourisme", décrivant l'ensemble des objets/priopriétés d'une offre touristique ;
* d'une base de connaissances constituant un référentiel unique pour les données nationales.

[En savoir plus sur l'ontologie](http://www.datatourisme.fr/ontologie/)

[Accès à l'ontologie](https://framagit.org/datatourisme/ontology/tree/master)