<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api;

use Datatourisme\Api\Resolver\BlazegraphResolver\BlazegraphResolver;
use Youshido\GraphQL\Execution\Processor;

class DatatourismeApi
{
    /** @var Processor $processor */
    private $processor;

    /**
     * DatatourismeApi constructor.
     *
     * @param $processor
     */
    public function __construct(Processor $processor)
    {
        $this->processor = $processor;
    }

    /**
     * @param $resolver
     *
     * @return DatatourismeApi
     */
    public static function create($resolver)
    {
        if (is_string($resolver)) {
            $resolver = new BlazegraphResolver($resolver);
        }

        return (new DatatourismeApiBuilder())
            ->setResolver($resolver)
            ->build();
    }

    /**
     * process payload.
     *
     * @param $payload
     *
     * @return array
     */
    public function process($payload, $variables = [])
    {
        return $this->processor->processPayload($payload, $variables)->getResponseData();
    }
}
