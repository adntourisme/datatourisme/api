<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\BlazegraphResolver;

use Datatourisme\Api\Resolver\BlazegraphResolver\Filter\Conditional\GeoBounding;
use Datatourisme\Api\Resolver\BlazegraphResolver\Filter\Conditional\GeoDistance;
use Datatourisme\Api\Resolver\BlazegraphResolver\Filter\Conditional\Text;
use Datatourisme\Api\Resolver\SparqlResolver\SparqlResolver;

class BlazegraphResolver extends SparqlResolver
{
    public static $GEO_PROPERTY = 'https://www.datatourisme.fr/ontology/core#latlon';

    /**
     * @return \Datatourisme\Api\Resolver\SparqlResolver\QueryBuilder\FilterGenerator
     */
    public function getFilterGenerator()
    {
        if (null == $this->filterGenerator) {
            $this->filterGenerator = parent::getFilterGenerator();
            $this->filterGenerator->addFilterClass(new Text());
            $this->filterGenerator->addFilterClass(new GeoDistance());
            $this->filterGenerator->addFilterClass(new GeoBounding());
            $this->setGeoProperty(self::$GEO_PROPERTY);
        }

        return $this->filterGenerator;
    }

    /**
     * @return \Datatourisme\Api\Resolver\SparqlResolver\QueryBuilder\QueryBuilderFactory
     */
    public function getQueryBuilderFactory()
    {
        if (null == $this->queryBuilderFactory) {
            $this->queryBuilderFactory = new BlazegraphQueryBuilderFactory();
        }

        return $this->queryBuilderFactory;
    }

    /**
     * @param $geoProperty
     *
     * @return $this
     */
    public function setGeoProperty($geoProperty)
    {
        $this->getFilterGenerator()->getFilter('_geo_bounding')->setGeoProperty($geoProperty);
        $this->getFilterGenerator()->getFilter('_geo_distance')->setGeoProperty($geoProperty);

        return $this;
    }
}
