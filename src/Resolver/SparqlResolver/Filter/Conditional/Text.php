<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Filter\Conditional;

use Datatourisme\Api\Resolver\SparqlResolver\Filter\ConditionalFilterInterface;
use Datatourisme\Api\Resolver\SparqlResolver\Sparql\Functions\Contains;
use Datatourisme\Api\Resolver\SparqlResolver\Sparql\Functions\Filter;
use Datatourisme\Api\Resolver\SparqlResolver\Utils\SparqlUtils;
use Datatourisme\Api\Schema\Field\AbstractField;

class Text implements ConditionalFilterInterface
{
    public function getName()
    {
        return '_text';
    }

    public function generate($subject, AbstractField $fieldDef, $value)
    {
        return new Filter(new Contains($subject.', '.SparqlUtils::formatFieldValue($value, $fieldDef)));
    }
}
