<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\QueryBuilder;

use Datatourisme\Api\Resolver\SparqlResolver\Collection\Collection;
use Datatourisme\Api\Resolver\SparqlResolver\Collection\FlatCollection;
use Datatourisme\Api\Resolver\SparqlResolver\Collection\OrderCollection;

class OrderClause
{
    /**
     * @var Collection
     */
    private $conditions;

    /**
     * @var FlatCollection
     */
    private $patterns;

    /**
     * OrderClause constructor.
     */
    public function __construct()
    {
        $this->conditions = new OrderCollection();
        $this->patterns = new FlatCollection();
    }

    /**
     * @return FlatCollection
     */
    public function getPatterns()
    {
        return $this->patterns;
    }

    /**
     * @param FlatCollection $patterns
     */
    public function setPatterns($patterns)
    {
        $this->patterns = $patterns;
    }

    /**
     * @return Collection
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * @param Collection $conditions
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
    }
}
