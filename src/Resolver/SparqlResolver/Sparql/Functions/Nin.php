<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql\Functions;

use Datatourisme\Api\Resolver\SparqlResolver\Sparql\Triplet;
use Datatourisme\Api\Resolver\SparqlResolver\Sparql\Uplet;
use Datatourisme\Api\Resolver\SparqlResolver\Sparql\UpletSuite;

class Nin extends Triplet
{
    public function __construct($subject, $tabObject)
    {
        parent::__construct();
        $this->_subject = new Uplet($subject);
        $this->_predicate = new Uplet('NOT IN');
        $this->_object = new UpletSuite($tabObject, ',');
    }

    public function __toString()
    {
        return $this->_subject.' '.$this->_predicate.' '.$this->_object;
    }
}
