<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql;

class Limit
{
    private $_limit;

    public function __construct($limit)
    {
        $this->_limit = $limit;
    }

    public function __toString()
    {
        return 'LIMIT '.$this->_limit;
    }
}
