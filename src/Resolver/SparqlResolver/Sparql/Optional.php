<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql;

class Optional
{
    private $_triplet;

    /**
     * Optional constructor.
     *
     * @param Triplet $triplet
     */
    public function __construct($triplet)
    {
        $this->_triplet = $triplet;
        $this->_triplet->setOptional(false);
    }

    public function __toString()
    {
        return 'OPTIONAL {'.(string) $this->_triplet.'}';
    }
}
