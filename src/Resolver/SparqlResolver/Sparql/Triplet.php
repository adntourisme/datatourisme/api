<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql;

class Triplet
{
    protected $_subject;
    protected $_predicate;
    protected $_object;
    protected $_optional;

    public function __construct($subject = null, $predicate = null, $object = null, $optional = false)
    {
        $this->_subject = new Uplet($subject);
        $this->_predicate = new Uplet($predicate);
        $this->_object = new Uplet($object);
        $this->setOptional($optional);
    }

    public function setOptional($optional)
    {
        $this->_optional = $optional;
    }

    public function __toString()
    {
        if ($this->_optional) {
            return (string) new Optional(new self($this->_subject, $this->_predicate, $this->_object));
        }

        return $this->_subject.' '.$this->_predicate.' '.$this->_object;
    }

    /**
     * @return Uplet
     */
    public function getSubject()
    {
        return $this->_subject;
    }

    /**
     * @return Uplet
     */
    public function getPredicate()
    {
        return $this->_predicate;
    }

    /**
     * @return Uplet
     */
    public function getObject()
    {
        return $this->_object;
    }
}
