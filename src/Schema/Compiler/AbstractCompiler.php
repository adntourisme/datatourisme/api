<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Schema\Compiler;

use Datatourisme\Api\Schema\PrefixMap;

abstract class AbstractCompiler
{
    /**
     * @var SchemaCompiler
     */
    protected $schemaCompiler;

    /**
     * @var PrefixMap
     */
    protected $prefixMap;

    /**
     * @var string
     */
    protected $content;

    /**
     * TypeCompiler constructor.
     *
     * @param SchemaCompiler $schemaCompiler
     * @param $schema
     *
     * @internal param $twig
     */
    public function __construct(SchemaCompiler $schemaCompiler, $schema)
    {
        $this->schemaCompiler = $schemaCompiler;
        $this->schema = $schema;
        $this->prefixMap = new PrefixMap($schema['prefixes']);
        $this->content = '';
    }

    abstract public function compile(): string;

    /**
     * @param $template
     * @param $className
     * @param array $params
     *
     * @return string
     */
    protected function render($template, $className, $params = []): string
    {
        $params = array_merge($params, [
            'className' => $className,
        ]);

        return $this->schemaCompiler->render($template, $params);
    }
}
