<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Schema\Type\InputObject;

use Youshido\GraphQL\Type\InputObject\AbstractInputObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class GeoDistanceType extends AbstractInputObjectType
{
    public function build($config)
    {
        $config
            ->addField('lng', [
                'type' => new StringType(),
                'resolve' => function ($value) {
                    return $value;
                },
            ])
            ->addField('lat', [
                'type' => new StringType(),
                'resolve' => function ($value) {
                    return $value;
                },
            ])
            ->addField('distance', [
                'type' => new StringType(),
                'resolve' => function ($value) {
                    return $value;
                },
            ]);
    }

    public function getDescription()
    {
        return 'Réalise une requête géographique sur la base de coordonnées et de distance.';
    }
}
