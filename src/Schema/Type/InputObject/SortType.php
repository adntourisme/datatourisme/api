<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Schema\Type\InputObject;

use Youshido\GraphQL\Type\InputObject\AbstractInputObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class SortType extends AbstractInputObjectType
{
    public function build($config)
    {
        $config
            ->addField('order', [
                'type' => new StringType(),
                'resolve' => function ($value) {
                    return $value;
                },
            ]);
    }

    public function getDescription()
    {
        return 'Permet d\'ordonner les résulats.';
    }
}
