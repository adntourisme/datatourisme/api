<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor;

use Datatourisme\Api\DatatourismeApi;
use Datatourisme\Api\DatatourismeApiBuilder;
use Datatourisme\Api\Resolver\BlazegraphResolver\BlazegraphResolver;
use PHPUnit\Framework\TestCase;

abstract class AbstractGraphQLTest extends TestCase
{
    protected static $endpoint = 'http://localhost:9999/blazegraph/namespace/kb/sparql';

    protected $api;

    /**
     * AbstractGraphQLTest constructor.
     */
    public function setUp(): void
    {
        //$this->api = DatatourismeApi::create(self::$endpoint);
        $resolver = new BlazegraphResolver(self::$endpoint);
        $this->api = (new DatatourismeApiBuilder())
            ->setCache(false)
            ->setResolver($resolver)
            ->build();
    }

    /**
     * @param $payload
     *
     * @return string
     */
    protected function queryGraqhQL($payload, $variables = [])
    {
        return json_encode($this->api->process($payload, $variables));
    }
}
