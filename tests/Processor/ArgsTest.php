<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor;

class ArgsTest extends AbstractGraphQLTest
{
    public function testUri()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                uri: "https://data.datatourisme.fr/31/27684064-5142-3a1f-ac20-9c1b656732c7"
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO267549641513"]}]}}}', $res);
    }

    public function testVariable()
    {
        $res = $this->queryGraqhQL('
        query getPoi($id: String) {
            poi(
                uri: $id
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }', ['id' => 'https://data.datatourisme.fr/31/27684064-5142-3a1f-ac20-9c1b656732c7']);
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO267549641513"]}]}}}', $res);
    }

    public function testLimit()
    {
        $res = $this->queryGraqhQL('
        {
                poi(
                    size: 1,
                    filters: [
                        { allowedPersons: {_gt: "20"} },
                        { allowedPersons: {_lt: "30"} }
                    ]
                )
                {
                    total,
                    results {
                        allowedPersons
                    }
                }
        }');
        $this->assertEquals('{"data":{"poi":{"total":2,"results":[{"allowedPersons":[25]}]}}}', $res);
    }

    public function testSortDesc()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                sort:[
                    { allowedPersons: { order: "desc" } },
                ]
                filters: [
                    { allowedPersons: {_gt: "10"} },
                    { allowedPersons: {_lt: "25"} }
                ]
            )
            {
                    total,
                    results {
                        allowedPersons
                    }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":3,"results":[{"allowedPersons":[20]},{"allowedPersons":[20]},{"allowedPersons":[15]}]}}}', $res);
    }

    public function testSortAsc()
    {
        $res = $this->queryGraqhQL('
        query global{
            poi(
                sort:[
                    { allowedPersons: { order: "asc" } },
                ]
                filters: [
                    { allowedPersons: {_gt: "10"} },
                    { allowedPersons: {_lt: "25"} }
                ]
            )
            {
                    total,
                    results {
                        allowedPersons
                    }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":3,"results":[{"allowedPersons":[15]},{"allowedPersons":[20]},{"allowedPersons":[20]}]}}}', $res);
    }
}
