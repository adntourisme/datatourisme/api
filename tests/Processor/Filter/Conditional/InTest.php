<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor\Filter\Conditional;

use Tests\Datatourisme\Api\Processor\AbstractGraphQLTest;

class InTest extends AbstractGraphQLTest
{
    public function testIn()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { allowedPersons: {_in: ["203","202"]} }
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO130946193200"]}]}}}', $res);
    }

    public function testInRdfType()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { rdf_type: {_in: ["https://www.datatourisme.fr/ontology/core#MusicEvent","https://www.datatourisme.fr/ontology/core#SportsEvent"]} }
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }');
        $res = json_decode($res, true);
        $this->assertEquals(557, $res['data']['poi']['total']);
    }
}
