<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor\Filter;

use Tests\Datatourisme\Api\Processor\AbstractGraphQLTest;

class FiltersTest extends AbstractGraphQLTest
{
    public function testEq()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { allowedPersons: {_eq: "30"} }
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }
        ');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO308495934640"]}]}}}', $res);
    }

    public function testGtLt()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { allowedPersons: {_gt: "29"}},
                    { allowedPersons: {_lt: "31"}}
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO308495934640"]}]}}}', $res);
    }

    public function testGteLte()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { allowedPersons: {_gte: "203"} },
                    { allowedPersons: {_lte: "210"} }
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO130946193200"]}]}}}', $res);
    }

    public function testNe()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { allowedPersons: {_gte: "30"} },
                    { allowedPersons: {_lte: "300"} },
                    { allowedPersons: {_ne: "203"} }
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO308495934640"]}]}}}', $res);
    }

    public function testNin()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { allowedPersons: {_gte: "25"} },
                    { allowedPersons: {_lte: "300"} },
                    { allowedPersons: {_nin: ["203", "25"]} }
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }
        ');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO308495934640"]}]}}}', $res);
    }

    public function testText()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                size: 1,
                filters: [
                    { hasDescription: {shortDescription: { _text : "trop" }}}
                ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO299667308000"]}]}}}', $res);
    }

    public function testMultiple()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { rdf_type: { _eq : "https://www.datatourisme.fr/ontology/core#PointOfInterest" }},
                  	{ rdf_type: { _eq : "https://www.datatourisme.fr/ontology/core#EntertainmentAndEvent" }}
                ]
            )
            {
                total,
                results {
                    rdf_type
                }
            }
        }');
        $res = json_decode($res, true);
        $this->assertEquals(1308, $res['data']['poi']['total']);
    }
}
