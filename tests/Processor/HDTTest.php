<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor;

use Datatourisme\Api\DatatourismeApi;
use Datatourisme\Api\Resolver\HdtResolver\HdtResolver;
use PHPUnit\Framework\TestCase;

class HDTTest extends TestCase
{
    public function testCount()
    {
        $resolver = new HdtResolver(__DIR__ . '/../dataset.hdt');
        $api = DatatourismeApi::create($resolver);
        $res = json_encode($api->process('
        {
            poi
            {
                total
            }
        }'));
        $this->assertEquals('{"data":{"poi":{"total":984}}}', $res);
    }
}
