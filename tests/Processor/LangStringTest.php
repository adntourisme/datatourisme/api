<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor;

class LangStringTest extends AbstractGraphQLTest
{
    public function testLangString()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                uri: "https://data.datatourisme.fr/31/27684064-5142-3a1f-ac20-9c1b656732c7"
            )
            {
                total,
                results {
                    rdfs_label {
                        value
                        lang
                    }
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"rdfs_label":[{"value":"Cransac Mediathec","lang":"en"},{"value":"M\u00e9diath\u00e8que de Cransac","lang":"fr"}]}]}}}', $res);
    }

    public function testLangStringArg()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                uri: "https://data.datatourisme.fr/31/27684064-5142-3a1f-ac20-9c1b656732c7",
                lang: "en"           
            ) {
                total,
                results {
                    rdfs_label {
                        value
                        lang
                    }
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"rdfs_label":[{"value":"Cransac Mediathec","lang":"en"}]}]}}}', $res);
    }

    public function testLangStringLegacy()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                uri: "https://data.datatourisme.fr/31/27684064-5142-3a1f-ac20-9c1b656732c7"
            )
            {
                total,
                results {
                    rdfs_label
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"rdfs_label":["Cransac Mediathec","M\u00e9diath\u00e8que de Cransac"]}]}}}', $res);
    }
}
