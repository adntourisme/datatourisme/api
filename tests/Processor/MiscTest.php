<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor;

use Datatourisme\Api\DatatourismeApiBuilder;
use Datatourisme\Api\Resolver\BlazegraphResolver\BlazegraphResolver;

class MiscTest extends AbstractGraphQLTest
{
    public function testDeep()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
            filters: [
                { dc_identifier: {_eq: "TFO308495934640" } }
            ]
            )
            {
                total,
                results {
                    isLocatedAt {
                        schema_geo {
                            schema_latitude
                        }
                    }
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"isLocatedAt":[{"schema_geo":[{"schema_latitude":[44.8299]}]}]}]}}}', $res);
    }

    public function testType()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { dc_identifier: {_eq: "TFO308495934640" } }
                ]
            )
            {
                total,
                results {
                    dc_identifier
                    isLocatedAt {
                        schema_geo{
                            schema_latitude
                        }
                    }
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"dc_identifier":["TFO308495934640"],"isLocatedAt":[{"schema_geo":[{"schema_latitude":[44.8299]}]}]}]}}}', $res);
    }

    public function testRdfType()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                    { dc_identifier: {_eq: "TFO308495934640" } }
                ]
            )
            {
                total,
                results {
                    hasDescription{
                        rdf_type
                    }
                }
            }
        }');
        $this->assertEquals('{"data":{"poi":{"total":1,"results":[{"hasDescription":[{"rdf_type":["https:\/\/www.datatourisme.fr\/ontology\/core#Description"]}]}]}}}', $res);
    }

    public function testRdfType2()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                size:1
                filters: [
                            {  rdf_type:{_eq:"https://www.datatourisme.fr/ontology/core#PlaceOfInterest"}}
                        ]
            )
            {
                total
            }
        }');
        $res = json_decode($res, true);
        $this->assertEquals(5, $res['data']['poi']['total']);
    }

    public function testTimeout()
    {
        $resolver = new BlazegraphResolver(self::$endpoint);
        $resolver->setTimeout(5);
        $api = (new DatatourismeApiBuilder())->setCache(false)->setResolver($resolver)->build();
        $res = $api->process('{ poi() { total } }');
        $this->assertEquals("Query timed out", $res['errors']['0']['message']);
    }
}
