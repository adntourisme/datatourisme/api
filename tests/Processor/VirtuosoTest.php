<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor;

use Datatourisme\Api\DatatourismeApi;
use Datatourisme\Api\Resolver\VirtuosoResolver\VirtuosoResolver;
use PHPUnit\Framework\TestCase;

class VirtuosoTest extends TestCase
{
    public function testCount()
    {
        $resolver = new VirtuosoResolver('http://localhost:8890/sparql');
        $api = DatatourismeApi::create($resolver);
        $res = json_encode($api->process('
        {
            poi
            {
                total
            }
        }'));
        $this->assertEquals('{"data":{"poi":{"total":1308}}}', $res);
    }

    public function testGraph()
    {
        $resolver = new VirtuosoResolver('http://localhost:8890/sparql');
        $resolver->setDefaultGraphUri('http://localhost:8890/DAV');
        $api = DatatourismeApi::create($resolver);
        $res = json_encode($api->process('
        {
            poi
            {
                total
            }
        }'));
        $this->assertEquals('{"data":{"poi":{"total":1308}}}', $res);
    }

    public function testBind()
    {
        $resolver = new VirtuosoResolver('http://localhost:8890/sparql');
        $api = DatatourismeApi::create($resolver);
        $res = json_encode($api->process('
        {
            poi(uri: "https://data.datatourisme.fr/31/6fe1d84a-b074-3f96-9033-98e4807476f5")
            {
                total
            }
        }'));
        $this->assertEquals('{"data":{"poi":{"total":1}}}', $res);
    }
}
